package maj.game.laboratories.sheep;

import com.jme3.light.Light;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

/**
 * extension of Node, 's gonna contain all controls, nodes, lights etc. in the game area
 * @author michcioperz <michcioperz@gmail.com>
 * @version 4
 * @since 3
 */
public class GameObject extends Node {
    private ClassConnector cxn;
    private final String objectName;
    
    /**
     * creates a GameObject and adds it to map at its name
     * as default, adds to root node
     * @param mainy ClassConnector to get the 
     * @param name proposed name for the objecr
     */
    public GameObject(ClassConnector mainy, String name) {
        this(mainy, name, true);
    }
    
    /**
     * creates a GameObject and adds it to map at its name
     * if bool is true, adds it to root node
     * @param mainy ClassConnector to get the 
     * @param name proposed name for the objecr
     * @param addToRootNode should I add it to Root Node?
     * @deprecated
     */
    public GameObject(ClassConnector mainy, String name, boolean addToRootNode) {
        this.cxn = mainy;
        while (this.cxn.getGameObjects().containsKey(name)) {
            name += ".";
        }
        this.cxn.getGameObjects().put(name, this);
        this.objectName = name;
        if (addToRootNode) {
            this.cxn.getRootNode().attachChild(this);
        }
    }
    
    /**
     * Light storage
     * @since 4
     */
    private Light light;
    
    /**
     * checks if Light exists
     * @return true, if light isn't null
     * @since 4
     */
    public boolean hasLight() {
        return light != null;
    }
    
    /**
     * gets the Light from the storage
     * @return Light stored privately
     * @since 4
     */

    public Light getLight() {
        return light;
    }
    
    /**
     * changes stored Light to a new one
     * also, removes old and adds new to the node
     * @param newLight the Light to replace the older one
     * @return true, if no exceptions happened
     * @since 4
     */
    public boolean setLight(Light newLight) {
        if (light != null) {
            cxn.getRootNode().removeLight(light);
        }
        try {
            light = newLight;
            cxn.getRootNode().addLight(light);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    
    /**
     * Spatial submodule storage
     * @since 4
     */
    private Spatial mesh;
    
    /**
     * checks if the mesh contains anything
     * @return true, if mesh isn't null
     * @since 4
     */
    public boolean hasMesh() {
        return mesh != null;
    }
    
    /**
     * gets the mesh from private storage
     * @return Spatial stored as mesh
     */
    public Spatial getMesh() {
        return mesh;
    }
    
    /**
     * changes stored mesh to a new one
     * also, detaches and attaches from/to node
     * @param newMesh new Spatial to replace the older one
     * @return true, if no exceptions happened
     */
    public boolean setMesh(Spatial newMesh) {
        if (mesh != null) {
            this.detachChild(mesh);
        }
        try {
            mesh = newMesh;
            this.attachChild(mesh);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
