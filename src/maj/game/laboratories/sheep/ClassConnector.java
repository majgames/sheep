package maj.game.laboratories.sheep;

import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioRenderer;
import com.jme3.audio.Listener;
import com.jme3.input.FlyByCamera;
import com.jme3.input.InputManager;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.Renderer;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.system.JmeContext;
import com.jme3.system.Timer;
import java.util.Map;

/**
 * lets Main share its components without sharing itself
 * @author michcioperz <michcioperz@gmail.com>
 * @version 3
 * @since 2
 */
public class ClassConnector {
    private final AssetManager assetManager;
    private final AudioRenderer audioRenderer;
    private final Camera camera;
    private final JmeContext context;
    private final FlyByCamera flyByCam;
    private final Node guiNode;
    private final ViewPort guiViewport;
    private final InputManager inputManager;
    private final Listener listener;
    private final Renderer renderer;
    private final RenderManager renderManager;
    private final Node rootNode;
    private final AppStateManager stateManager;
    private final Timer timer;
    private final ViewPort viewport;
    
    private final Map<String, GameObject> objects;
    
    /**
     * gets all the managers from Main and saves links to private fields
     * @param app an instance of Main
     */
    public ClassConnector(Main app) {
        this.assetManager = app.getAssetManager();
        this.audioRenderer = app.getAudioRenderer();
        this.camera = app.getCamera();
        this.context = app.getContext();
        this.flyByCam = app.getFlyByCamera();
        this.guiNode = app.getGuiNode();
        this.guiViewport = app.getGuiViewPort();
        this.inputManager = app.getInputManager();
        this.listener = app.getListener();
        this.renderer = app.getRenderer();
        this.renderManager = app.getRenderManager();
        this.rootNode = app.getRootNode();
        this.stateManager = app.getStateManager();
        this.timer = app.getTimer();
        this.viewport = app.getViewPort();
        
        this.objects = app.getGameObjects();
    }
    
    /**
     * gets AssetManager from private field
     * @return AssetManager from Main
     */
    public AssetManager getAssetManager() {
        return this.assetManager;
    }
    
    /**
     * gets AudioRenderer from private field
     * @return AudioRenderer from Main
     */
    public AudioRenderer getAudioRenderer() {
        return this.audioRenderer;
    }
    
    /**
     * gets Camera from private field
     * @return Camera from Main
     */
    public Camera getCamera() {
        return this.camera;
    }
    
    /**
     * gets JmeContext from private field
     * @return JmeContext from Main
     */
    public JmeContext getContext() {
        return this.context;
    }
    
    /**
     * gets FlyByCamera from private field
     * @return FlyByCamera from Main
     */
    public FlyByCamera getFlyByCamera() {
        return this.flyByCam;
    }
    
    /**
     * gets GUI Node from private field
     * @return Node used by Main for displaying GUI
     */
    public Node getGuiNode() {
        return this.guiNode;
    }
    
    /**
     * gets GUI ViewPort from private field
     * @return ViewPort used by Main for displaying GUI
     */
    public ViewPort getGuiViewport() {
        return this.guiViewport;
    }
    
    /**
     * gets InputManager from private field
     * @return InputManager from Main
     */
    public InputManager getInputManager() {
        return this.inputManager;
    }
    
    /**
     * gets Listener from private field
     * @return Listener from Main
     */
    public Listener getListener() {
        return this.listener;
    }
    
    /**
     * gets Renderer from private field
     * @return Renderer from Main
     */
    public Renderer getRenderer() {
        return this.renderer;
    }
    
    /**
     * gets RenderManager from private field
     * @return RenderManager from Main
     */
    public RenderManager getRenderManager() {
        return this.renderManager;
    }
    
    /**
     * gets Root Node from private field
     * @return Node used by Main as scene's highest point in hierarchy
     */
    public Node getRootNode() {
        return this.rootNode;
    }
    
    /**
     * gets AppStateManager from private field
     * @return AppStateManager from Main
     */
    public AppStateManager getStateManager() {
        return this.stateManager;
    }
    
    /**
     * gets Timer from private field
     * @return Timer from Main
     */
    public Timer getTimer() {
        return this.timer;
    }
    
    /**
     * gets ViewPort from private field
     * @return ViewPort from Main
     */
    public ViewPort getViewPort() {
        return this.viewport;
    }
    
    /**
     * gets GameObject map
     * @return Main's (Hash)Map<String, GameObject>
     * @since 3
     */
    public Map<String, GameObject> getGameObjects() {
        return this.objects;
    }
}