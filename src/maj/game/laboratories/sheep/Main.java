package maj.game.laboratories.sheep;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import java.util.HashMap;
import java.util.Map;

/**
 * engine's main code, place where magic happens
 * @author michcioperz <michcioperz@gmail.com>
 * @version 3
 * @since 1
 */
public abstract class Main extends SimpleApplication {
    
    private Map<String, GameObject> objects = new HashMap<String, GameObject>();
    
    private ClassConnector classConnector;
    
    /**
     * gets a map of GameObjects constructed yet
     * @return (Hash)Map<String, GameObject> with all objects created yet
     * @since 3
     */
    public Map<String, GameObject> getGameObjects() {
        return this.objects;
    }
    
    /**
     * gets ClassConnector for access to Main's all subcomponents without access to Main itself
     * @return instance's ClassConnector
     */
    public ClassConnector exchangeMain() {
        return classConnector;
    }

    /**
     * what to do on start, like adding things to the Root Node
     */
    @Override
    public void simpleInitApp() {
        this.classConnector = new ClassConnector(this);
    }

    /**
     * keep the things running
     * @param tpf time per frame - how many miliseconds passed from last update
     */
    @Override
    public abstract void simpleUpdate(float tpf);

    /**
     * I have no idea what I should use it for
     * @param rm Main's RenderManager
     */
    @Override
    public abstract void simpleRender(RenderManager rm);
}
